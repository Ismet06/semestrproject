
package DBConnection;

import java.sql.*;


public class ChooseGroupTimeTable {
    
    ConnectDB dbcon = new ConnectDB();
    Statement st = null;
    ResultSet rs = null;
    public ResultSet groupTimetable(String groupName){
            
        try {
            dbcon.connect();
            st = dbcon.con.createStatement();
            rs=st.executeQuery("SELECT * FROM timetable WHERE group_name='"+groupName+ "'");
          
            return rs;
        } catch (SQLException e) {
            System.out.println("Didn't find such column ->  "+groupName+e);
        }
        return null;
    }
//    public static void main(String[] args) throws SQLException {
//        ChooseGroupTimeTable co = new ChooseGroupTimeTable();
//       ResultSet rs1=co.groupTimetable("Eng23");
//      while(rs1.next())
//        System.out.println(rs1.getString("teacher_name"));
//        
//    }

}
