package DBConnection;

import java.sql.Connection;
import java.sql.*;

public class ConnectDB {

    String url = "jdbc:mysql://localhost:3306/facultysitedb";
    String user = "root";
    String pass = "ismet.22";

    Connection con;
    public void connect() {

        try {
            Class.forName("com.mysql.jdbc.Driver");
            con = DriverManager.getConnection(url, user, pass);
        } catch (SQLException e) {
            System.out.println("failed connecting to db!!" + e);
        } catch (ClassNotFoundException ex) {
            System.out.println("driver exception        " + ex);
        }
    }

    public ResultSet chooseTable() {

        connect();
        try {
            Statement st = con.createStatement();
            ResultSet rs = st.executeQuery("SELECT group_name FROM timetable GROUP BY group_name");
            return rs;
        } catch (SQLException e) {
        }
        return null;
    }
//    public static void main(String[] args) throws SQLException {
//        ConnectDB c = new ConnectDB();
//        ResultSet rs =c.chooseTable();
//       while(rs.next()){
//           System.out.println(rs.getString("group_name"));
//       }
//    }
}
