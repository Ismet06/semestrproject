
package DBConnection;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

public class AppMath {
        
    ConnectDB dbc = new ConnectDB();
    
    Statement sta=null;
    ResultSet rse = null;
    public ResultSet headers(){
            try {
            dbc.connect();
            sta=dbc.con.createStatement();
            rse=sta.executeQuery("SELECT header FROM app_math");
            return rse;
        } catch (SQLException e) {
                System.out.println("connection failed !!!  " +e);
        }
            return null;
    }
    public ResultSet infos(){
        try {
            dbc.connect();
            sta=dbc.con.createStatement();
            return rse=sta.executeQuery("SELECT header, info FROM app_math");
        } catch (SQLException e) {
            System.out.println("app info exception    " + e);
        }
        return null;
    }
//    public static void main(String[] args) throws SQLException {
//        AppMath a = new AppMath();
//        ResultSet s = a.headers();
//        while(s.next()){
//            System.out.println(s.getString("header"));
//        }
//    }
}
