
package DBConnection;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;


public class Tutorials {
    
    ConnectDB dbc = new ConnectDB();
    
    Statement st=null;
    ResultSet rs=null;
    public ResultSet tutorialTable(){
        try {
            dbc.connect();
            st=dbc.con.createStatement();
            rs=st.executeQuery("SELECT * FROM tutorials ");
            return rs;
        } catch (SQLException e) {
        }
        return null;
    }
 
    public ResultSet choosenTutorial(String tutorialName){
        try {
            dbc.connect();
            st=dbc.con.createStatement();
            rs=st.executeQuery("SELECT topic_name, topic_txt FROM java_tutorial");
            return rs;
        } catch (SQLException e) {
        }
        return null;
    }
//    public static void main(String[] args) throws SQLException {
//        Tutorials t = new Tutorials();
//        ResultSet s = t.tutorialTable();
//        while(s.next()){
//            System.out.println(s.getString("tutorialsname"));
//            break;
//        }
//    }
}
