package DBConnection;

import java.sql.*;

public class BSU {

    ConnectDB dbconn = new ConnectDB();
    Statement st = null;
    ResultSet rs = null;

    public ResultSet headers() {
        
        try {
            dbconn.connect();
            st = dbconn.con.createStatement();
            rs = st.executeQuery("SELECT header FROM aboutbsu");
        return rs;
        } catch (SQLException e) {
            System.out.println("Database connection failed!!!  "+e);
        }
        return null;
    }
    public ResultSet infos(){
      
        try {
            dbconn.connect();
            st=dbconn.con.createStatement();
            return rs=st.executeQuery("SELECT info, header FROM aboutbsu");
        } catch (SQLException e) {
            System.out.println("infos exception   " + e);
        }
        return null;
    }
//    public static void main(String[] args) throws SQLException {
//        BSU bsu = new BSU();
//       ResultSet rs1 = bsu.headers();
//       while(rs1.next()){
//           System.out.println(rs1.getInt("header"));
//           break;
//       }
//    }
}
