
<%-- 
    Document   : TimTable
    Created on : May 16, 2017, 6:36:43 PM
    Author     : Ismet
--%>
<%@page import="DBConnection.*"%>

<%@page import= "java.sql.*" %>
<% Class.forName("com.mysql.jdbc.Driver");%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <style>
            
        </style>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/Style/style.css" />
        <title>Schedule</title>
    </head>
    <body>
        <div id="page">
            <div id="header">
                <div id="section">
                    <div><a href="index.html"><img src="<%=request.getContextPath()%>/Images/mylogo.png" alt="" /></a></div>
                </div>
                <ul>
                    <li><a href="bsu.jsp">BSU</a></li>
                    <li><a href="AppMath.jsp">Applied Math</a></li>
                    <li><a href="TutorialPage.jspl">Tutorials</a></li>
                    <li class="current"><a href="Timetable.jsp">Schedule</a></li>
                </ul>
            </div>
            <div id="content">
                <div>
                    <h3>Timetable</h3>
                    <% ConnectDB con = new ConnectDB();
                        ResultSet rs = con.chooseTable();
                    %>
                    <h4>Choose your group: </h4>  
                    <form action="Timetable.jsp" method="POST">
                        <select name="choosenGroup" id="choosenGroup">
                            <%while (rs.next()) {%>
                            <option value=<%= rs.getString("group_name")%>><%= rs.getString("group_name")%></option>
                            <% }%>
                        </select>
                        <input type="submit" value="submit" name="submit"/>
                    </form>

                    <%
                        if (request.getParameter("submit") != null) {
                            ChooseGroupTimeTable cg = new ChooseGroupTimeTable();
                            ResultSet rsg = cg.groupTimetable(request.getParameter("choosenGroup"));

                    %>


                    <table border="1">
                        <thead>
                            <tr>
                                <th class="th1"><font color="red">Days</font></th>
                                <th align="center"><font color="red">MON</font></th>
                                <th align="center"><font color="red">TUE</font></th>
                                <th align="center"><font color="red">WED</font></th>
                                <th align="center"><font color="red">THU</font></th>
                                <th align="center"><font color="red">FRI</font></th>
                            </tr>
                        </thead>
                        <tbody>
                            <tr>
                                <td><font color="blue">1st lesson</font></td>

                                <%while (rsg.next()) {
                                        if (rsg.getInt("subject_priority") == 1) {
                                %>
                                <td align="center"><%= rsg.getString("subject")%> <br>
                                    <%= rsg.getString("teacher_name")%><br>
                                    <font color="red"><%= rsg.getInt("room")%></font>
                                </td>
                                <%}
                                    }%>
                            </tr>
                            <tr>
                                <td align="center"><font color="blue">2nd lesson</blue></td>
                                    <%rsg = cg.groupTimetable(request.getParameter("choosenGroup"));
                                        while (rsg.next()) {
                                            if (rsg.getInt("subject_priority") == 2) {
                                    %>
                                <td align="center" ><%= rsg.getString("subject")%> <br>
                                    <%= rsg.getString("teacher_name")%><br>
                                    <font color="red"> <%= rsg.getInt("room")%></font>
                                </td>
                                <%}
                                    }%>
                            </tr>
                            <tr>
                                <%String[] days = {"MON", "TUE", "WED", "THU", "FRI"};%>
                                <td align="center"><font color="blue">3rd lesson</font></td>
                                    <%rsg = cg.groupTimetable(request.getParameter("choosenGroup"));
                                        while (rsg.next()) {

                                            if (rsg.getInt("subject_priority") == 3) {
                                                for (int i = 0; i < days.length; i++) {
                                                    if (rsg.getString("datetime").equals(days[i])) {
                                    %>
                                <td align="center"><%= rsg.getString("subject")%> <br>
                                    <%= rsg.getString("teacher_name")%><br>
                                    <font color="red"> <%= rsg.getInt("room")%></font>
                                </td>
                                <% } else {%>

                                <%}
                                            }
                                        }
                                    }%>
                            </tr>
                            <%}%>
                        </tbody>
                    </table>

                </div>
            </div>
            <div id="footer">
                <div>
                    <div class="section">
                        <ul>
                            <li><a href="bsu.jsp">BSU</a></li>
                            <li><a href="AppMath.jsp">Applied Math</a></li>
                            <li><a href="TutorialPage.jsp">Computer Programming</a></li>
                            <li><a href="Timetable.jsp">Schedule</a></li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </body>
</html>
