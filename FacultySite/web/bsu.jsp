<%-- 
    Document   : bsu
    Created on : May 24, 2017, 6:57:30 PM
    Author     : Ismer
--%>

<%@page import="java.sql.*"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@page import="DBConnection.*"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/Style/style.css" />
        <!--[if IE 6]><link rel="stylesheet" type="text/css" href="styles/ie6.css" /><![endif]-->
        <title>About BSU</title>
        <style>
            .page-title-toc{background:#FAFAFA;padding:20px 0;box-shadow:0 0 2px #2C394B;}
            .page-title-toc h2{font-size:1.1em;text-transform:uppercase;text-align:center;margin:0;}
            .page-title-toc ul{list-style:none;padding-left:25px;margin:18px 0 0 0;font-family:Tahoma,"Trebuchet MS",'Open Sans';}
            .page-title-toc ul li{line-height:35px;}
            .page-title-toc ul li:before{word-spacing:5px;content:"-- ";}
            .page-title-toc{background:#FAFAFA;padding:20px 0;box-shadow:0 0 2px #2C394B;}
            .page-title-toc h2{font-size:1.1em;text-transform:uppercase;text-align:center;margin:0;}
            .page-title-toc ul{list-style:none;padding-left:25px;margin:18px 0 0 0;font-family:Tahoma,"Trebuchet MS",'Open Sans';}
            .page-title-toc ul li{line-height:35px;}
            .page-title-toc ul li:before{word-spacing:5px;content:"-- ";}
            .page-title-toc{margin-bottom:25px;}
            .page-title-toc{float:right;width:300px;}
            .page-title-toc ul li{line-height:32px;}
            .page-title-toc {
                background: #FAFAFA;
                background-color: rgb(250, 250, 250);
                background-image: none;
                background-repeat: repeat;
                background-attachment: scroll;
                background-clip: border-box;
                background-origin: padding-box;
                background-position-x: 0%;
                background-position-y: 0%;
                background-size: auto auto;
                padding: 20px 0;
                padding-top: 20px;
                padding-right: 0px;
                padding-bottom: 20px;
                padding-left: 0px;
                box-shadow: 0 0 2px #2C394B;
            }
        </style>
    </head>
    <body>
        <div id="page">
            <div id="header">
                <div id="section">
                    <div><img src="<%=request.getContextPath()%>/Images/logo.png" alt="" /></a></div>
                </div>
                <ul>
                    <li class="current"><a href="bsu.jsp">BSU</a></li>
                    <li><a href="AppMath.jsp">Applied Math</a></li>
                    <li><a href="TutorialPage.jsp">Tutorials</a></li>
                    <li><a href="Timetable.jsp">Schedule</a></li>
                </ul>
            </div>
            <div id="content">
                <div>
                    <h3>Baku State University</h3>

                    <div class="first"> <a href="#"><img src="<%=request.getContextPath()%>/Images/bsu.jpg" alt="" width="320" height="250" /></a>
                        <div class="page-title-toc">
                            <%
                                BSU bsu = new BSU();
                                ResultSet rs = bsu.headers();
                            %>
                            <h2>Table of Contents</h2>
                            <%while (rs.next()) {%>
                            <ul>
                                <li><a style="color:blue" href="#<%= rs.getString("header")%>"><%= rs.getString("header")%></a></li>
                            </ul>
                            <%}%>
                        </div>
                    </div>
                    <div>
                        <%ResultSet rsset = bsu.infos();
                            while (rsset.next()) {
                                if (rsset.getString("header").equals("History of BSU")) {%>
                        <h2><%= rsset.getString("header")%></h2>
                        <p id="<%= rsset.getString("header")%>"><%= rsset.getString("info")%></p>
                        <%}
                            if (rsset.getString("header").equals("Establishing of BSU")) {%>
                        <h2><%= rsset.getString("header")%></h2>
                        <p id="<%= rsset.getString("header")%>"><%= rsset.getString("info")%></p>
                        <%}
                            if (rsset.getString("header").equals("Baku State University in the 1930s")) {%>
                        <h2><%= rsset.getString("header")%></h2>
                        <p id="<%= rsset.getString("header")%>"><%= rsset.getString("info")%></p>
                        <%
                            }
                            if (rsset.getString("header").equals("Renaissance stage in the development of the university(1969-1968)")) {%>
                        <h2><%= rsset.getString("header")%></h2>
                        <p id="<%= rsset.getString("header")%>"><%= rsset.getString("info")%></p>
                        <%}
                            if (rsset.getString("header").equals("University during the early years of independence and new development")) {%>
                        <h2><%= rsset.getString("header")%></h2>
                        <p id="<%= rsset.getString("header")%>"><%= rsset.getString("info")%></p>
                        <%}
                            }%>
                    </div>
                </div>
            </div>
            <div id="footer">
                <div>
                    <div class="section">
                        <ul>
                            <li><a href="bsu.jsp">BSU</a></li>
                            <li><a href="AppMath.jsp">Applied Math</a></li>
                            <li><a href="TutorialPage.jsp">Computer Programming</a></li>
                            <li><a href="Timetable.jsp">Schedule</a></li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </body>
</html>
