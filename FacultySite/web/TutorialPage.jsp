<%-- 
    Document   : Tutorial
    Created on : May 24, 2017, 7:07:08 PM
    Author     : Ismer
--%>

<%@page import="java.sql.ResultSet"%>
<%@page import="DBConnection.Tutorials"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <style>
            .page-title-toc{background:#FAFAFA;padding:20px 0;box-shadow:0 0 2px #2C394B;}
            .page-title-toc h2{font-size:1.1em;text-transform:uppercase;text-align:center;margin:0;}
            .page-title-toc ul{list-style:none;padding-left:25px;margin:18px 0 0 0;font-family:Tahoma,"Trebuchet MS",'Open Sans';}
            .page-title-toc ul li{line-height:35px;}
            .page-title-toc ul li:before{word-spacing:5px;content:"-- ";}
            .page-title-toc{background:#FAFAFA;padding:20px 0;box-shadow:0 0 2px #2C394B;}
            .page-title-toc h2{font-size:1.1em;text-transform:uppercase;text-align:center;margin:0;}
            .page-title-toc ul{list-style:none;padding-left:25px;margin:18px 0 0 0;font-family:Tahoma,"Trebuchet MS",'Open Sans';}
            .page-title-toc ul li{line-height:35px;}
            .page-title-toc ul li:before{word-spacing:5px;content:"-- ";}
            .page-title-toc{margin-bottom:25px;}
            .page-title-toc{float:right;width:300px;}
            .page-title-toc ul li{line-height:32px;}
            .page-title-toc {
                background: #FAFAFA;
                background-color: rgb(250, 250, 250);
                background-image: none;
                background-repeat: repeat;
                background-attachment: scroll;
                background-clip: border-box;
                background-origin: padding-box;
                background-position-x: 0%;
                background-position-y: 0%;
                background-size: auto auto;
                padding: 20px 0;
                padding-top: 20px;
                padding-right: 0px;
                padding-bottom: 20px;
                padding-left: 0px;
                box-shadow: 0 0 2px #2C394B;
            }
        </style>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/Style/style.css" />
        <!--[if IE 6]><link rel="stylesheet" type="text/css" href="styles/ie6.css" /><![endif]-->
        <title>Tutorials </title>
    </head>
    <body>
        <div id="page">
            <div id="header">
                <div id="section">
                    <div><img src="<%=request.getContextPath()%>/Images/mylogo.png" alt="" /></a></div>
                </div>
                <ul>
                    <li><a href="bsu.jsp">BSU</a></li>
                    <li><a href="AppMath.jsp">Applied Math</a></li>
                    <li class="current"><a href="TutorialPage.jsp">Tutorials</a></li>
                    <li><a href="Timetable.jsp">Schedule</a></li>
                </ul>
            </div>
            <div id="content">
                <div>
                    <h3>Computer Programming</h3>
                    <h2>INTRODUCTION  TO JAVA PROGRAMMING LANGUAGE</h2>
                    <p>Java is a simple and yet powerful object oriented programming language and it is in many respects similar to C++.
                        Java originated at Sun Microsystems, Inc. in 1991. It was conceived by James Gosling, Patrick Naughton, Chris Warth,
                        Ed Frank, and Mike Sheridan at Sun Microsystems, Inc. It was developed to provide a platform-independent programming language.
                        This site gives you an Introduction to Java Programming accompanied with many java examples. 
                        Its a complete course in java programming for beginners to advanced java.
                    </p>
                    <h2>PLATFORM INDEPENDENT</h2>
                    <p>Unlike many other programming languages including C and C++ when Java is compiled,
                        it is not compiled into platform specific machine, rather into platform independent byte code.
                        This byte code is distributed over the web and interpreted by virtual Machine (JVM) on whichever platform it is being run.
                        What is the Java Virtual Machine? What  is its role?

                        Java was designed with a concept of ‘write once and run everywhere’. 
                        Java Virtual Machine plays the central role in this concept.
                        The JVM is the environment in which Java programs execute. 
                        It is a software that is implemented on top of real hardware and operating system. 
                        When the source code (.java files) is compiled, it is translated into byte codes and then placed into (.class) files.
                        The JVM executes these bytecodes. So Java byte codes can be thought of as the machine language of the JVM. 
                        A JVM can either interpret the bytecode one instruction at a time or the bytecode can be compiled further for 
                        the real microprocessor using what is called a just-in-time compiler. The JVM must be implemented on a particular
                        platform before compiled programs can run on that platform.
                    </p>
                    <div class="section">
                        <%
                            Tutorials t = new Tutorials();
                            ResultSet rs = t.tutorialTable();
                        %>
                        <div id="aside">

                            <h4>Choose some tutorial to beginning curse!!!</h4 >
                            <div class="page-title-toc">
                                <%
                                        while (rs.next()) {%>
                                <ul>
                                    <li><span><a style="color:blue" href="TutorialPage.jsp?form=<%=rs.getString("tutorialsname")%>">
                                                <%= rs.getString("tutorialsname")%></a></span></li>
                                </ul>
                                <%}%>
                            </div>
                            <%
                                if (request.getParameter("form") != null) {
                                    System.out.println(request.getParameter("form"));
                                    ResultSet rss = t.choosenTutorial(request.getParameter("java_tutorial"));
                                    while (rss.next()) {
                            %>
                            <h2><%= rss.getString("topic_name")%></h2>
                            <p><%= rss.getString("topic_txt")%></p>
                            <%}
                                    }%>
                        </div>
                    </div>
                </div>
            </div>
            <div id="footer">
                <div>
                    <div class="section">
                        <ul>
                            <li><a href="bsu.jsp">BSU</a></li>
                            <li><a href="AppMath.jsp">Applied Math</a></li>
                            <li><a href="TutorialPage.jsp">Computer Programming</a></li>
                            <li><a href="Timetable.jsp">Schedule</a></li>
                        </ul>

                    </div>
                </div>
            </div>
        </div>
    </body>
</html>
