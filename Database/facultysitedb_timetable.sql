-- MySQL dump 10.13  Distrib 5.7.12, for Win64 (x86_64)
--
-- Host: localhost    Database: facultysitedb
-- ------------------------------------------------------
-- Server version	5.7.17-log

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `timetable`
--

DROP TABLE IF EXISTS `timetable`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `timetable` (
  `idtable` int(11) NOT NULL AUTO_INCREMENT,
  `datetime` varchar(10) NOT NULL,
  `room` int(11) NOT NULL,
  `teacher_name` varchar(45) NOT NULL,
  `group_name` varchar(45) NOT NULL,
  `subject` varchar(45) NOT NULL,
  `subject_priority` int(11) NOT NULL,
  PRIMARY KEY (`idtable`)
) ENGINE=InnoDB AUTO_INCREMENT=28 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `timetable`
--

LOCK TABLES `timetable` WRITE;
/*!40000 ALTER TABLE `timetable` DISABLE KEYS */;
INSERT INTO `timetable` VALUES (1,'MON',200,'Rafael M.','Eng23','Game Theory',1),(2,'MON',200,'Rena R.','Eng23','Civil defense',2),(3,'TUE',301,'Nicat S.','Eng23','Java programming',1),(4,'TUE',213,'Naile N.','Eng23','Algorithm theory',3),(5,'TUE',213,'Rena R.','Eng23','Civil defense',2),(6,'WED',200,'Sefiqe  M.','Eng23','Econometrica',2),(7,'WED',301,'Nicat S.','Eng23','Java programming',1),(8,'WED',200,'Rafael R.','Eng23','Game Theory',3),(9,'THU',200,'Yelena M.','Eng23','Numerical Method',1),(10,'THU',213,'Nicat S.','Eng23','Java Programming',2),(11,'FRI',213,'Yelena M.','Eng23','Numerical Method',1),(12,'FRI',213,'Sefiqe','Eng23','Econometrica ',3),(13,'FRI',213,'Naile A.','Eng23','Algorithm theory',2),(14,'MON',313,'Rakib E.','Tk46','Riyazi analiz',1),(15,'MON',313,'Nailr E.','Tk46','opyimallashdirma',2),(16,'TUE',313,'Yelena E.','Tk46','modellesdirme',1),(17,'TUE',313,'Rafael T.','Tk46','Riyazi analiz',2),(18,'WED',313,'Ravid D.','Tk46','Riyazi analiz',1),(19,'WED',313,'Ramil M.','Tk46','Riyazi analiz',3),(20,'WED',313,'Rafik A.','Tk46','Riyazi analiz',2),(21,'THU',313,'Ramil E.','Tk46','Riyazi analiz',1),(22,'THU',313,'Rakib E.','Tk46','Riyazi analiz',2),(23,'FRI',313,'Rakib E.','Tk46','Riyazi analiz',1),(24,'FRI',313,'Rakib E.','Tk46','Riyazi analiz',2),(25,'FRI',313,'Rakib E.','Tk46','Riyazi analiz',3),(26,'MON',313,'Rakib E.','Tk43','Riyazi analiz',1),(27,'THU',313,'Rakib E.','Tk44','Riyazi analiz',1);
/*!40000 ALTER TABLE `timetable` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2017-05-25 10:31:31
