-- MySQL dump 10.13  Distrib 5.7.12, for Win64 (x86_64)
--
-- Host: localhost    Database: facultysitedb
-- ------------------------------------------------------
-- Server version	5.7.17-log

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `java_tutorial`
--

DROP TABLE IF EXISTS `java_tutorial`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `java_tutorial` (
  `idjava_t` int(11) NOT NULL AUTO_INCREMENT,
  `tutorial_id` int(11) NOT NULL,
  `topic_name` varchar(45) NOT NULL,
  `topic_txt` varchar(600) NOT NULL,
  PRIMARY KEY (`idjava_t`),
  KEY `tutorial_id_idx` (`tutorial_id`),
  CONSTRAINT `tutorial_id` FOREIGN KEY (`tutorial_id`) REFERENCES `tutorials` (`tutorial_id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `java_tutorial`
--

LOCK TABLES `java_tutorial` WRITE;
/*!40000 ALTER TABLE `java_tutorial` DISABLE KEYS */;
INSERT INTO `java_tutorial` VALUES (1,1,'Introduction','JAVA was developed by Sun Microsystems Inc in 1991, later acquired by Oracle Corporation. It was conceived by James Gosling and Patrick Naughton. It is a simple programming language.  Writing, compiling and debugging a program is easy in java.  It helps to create modular programs and reusable code.To understand the meaning of platform independent, we must need to understand the meaning of platform first. A platform is a pre-existing environment in which a program runs, obeying its constraints, and making use of its facilities.'),(2,1,'Java if, if-else','In this article, you will learn to use two selection statements: if and if...else to control the flow of your program’s execution. if (expression) {'),(3,1,'Java For loop','Loop is used in programming to repeat a specific block of code until certain condition is met (test expression is false). The syntax of for Loop in Java is:');
/*!40000 ALTER TABLE `java_tutorial` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2017-05-25 10:31:29
