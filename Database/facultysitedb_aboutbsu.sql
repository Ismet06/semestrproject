-- MySQL dump 10.13  Distrib 5.7.12, for Win64 (x86_64)
--
-- Host: localhost    Database: facultysitedb
-- ------------------------------------------------------
-- Server version	5.7.17-log

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `aboutbsu`
--

DROP TABLE IF EXISTS `aboutbsu`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `aboutbsu` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `content` varchar(45) NOT NULL,
  `header` varchar(100) NOT NULL,
  `info` varchar(1000) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `aboutbsu`
--

LOCK TABLES `aboutbsu` WRITE;
/*!40000 ALTER TABLE `aboutbsu` DISABLE KEYS */;
INSERT INTO `aboutbsu` VALUES (1,'BSU','History of BSU','At the meeting of the Parliament of the Democratic Republic of Azerbaijan on September 1, 1919, a decision was made to establish a university in Baku and its constitution was approved - a new higher education camp was formed at the crossroads of Europe and Asia. The university started its first year with 2 faculties - history-philology, medical faculties and 1094 students. The first rector of the university was Kazan University professor, famous surgeon V.I.Razumovski. '),(2,'BSU','Establishing  of BSU','Baku State University was founded on September 1, 1919 by the parliament of the People\'s Republic of Azerbaijan. National leader Haydar Aliyev said that it was a very important historical event, such as the creation of the BDU, and that it was one of the most important events in the history of our people to establish a university that sheds light on Azerbaijani science since the turn of the century. The Azerbaijani people, who had been forced to lose their independence and tattooism for a long time because of historical injustice, were deprived of the possibility of creating a national education system and moral education methods. Only a few years after our independence in 1918 and declaring the People\'s Republic of Azerbaijan, it became possible to evaluate the role of science and education in national and social development. '),(3,'BSU','Baku State University in the 1930s','In 1929, it was decided that the university would be celebrated on the ümumrespublika heights, and this measure was made in January 1930 in a magnificent manner. Immediately after that, complex and dark events began in the life of the university. In June 1930, the Azerbaijani State of the Republic of Azerbaijan was ceased to operate under the Soviet of the People\'s Commissars of the SSC, and new independent institutions were established on the basis of its separate faculties. Although the opening of new institutes in the country was a step forward in the development of higher education in the life of the country, in fact, there were deeper problems behind it. These precautions would be to spend the day without thinking about the university. '),(4,'BSU','Renaissance stage in the development of the university(1969-1968)','The new quality stage in the development of the university is undoubtedly related to the name and activities of the national leader of Azerbaijani people, BDU-nun History Faculty graduate Haydar Aliyev.'),(5,'BSU','University during the early years of independence and new development','In the first years of independence, Azerbaijan, especially in the life of the university, shows a moment of turmoil and misery. In 1989-1993, the rector was changed 5 times in BDU: 1987-1989 AEA\'s correspondent member, prof. Mammadov, son of Yahya Cafer; Between 1989 and 1990, academic Cemil Bahadur son Guliyev; In 1990-1992 he was the son of academic Mirabbas Gökçe Qasimov; In 1992-1993 prof. Firudin Yusif son Səməndərov.');
/*!40000 ALTER TABLE `aboutbsu` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2017-05-25 10:31:31
